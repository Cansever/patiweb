from django.db import models

# Create your models here.
from core.models import BaseModel
from django.utils.translation import ugettext as _
from django.contrib.postgres.fields.jsonb import JSONField as JSONBField

class AdoptPost(BaseModel):

    MALE = 1
    FEMALE = 2
    GENDER_CHOICES = (
        (MALE, _('Erkek',),),
        (FEMALE, _('Dişi',),),
    )

    CAT = 1
    DOG = 2
    BIRD = 3
    FISH = 4
    REPTILE = 5
    RODENT = 6
    RABBIT =7
    TYPE_CHOICES = (
        (CAT, _('Kedi',),),
        (DOG, _('Köpek',),),
        (BIRD, _('Kuş',),),
        (FISH, _('Balık',),),
        (REPTILE, _('Sürüngen',),),
        (RODENT, _('Kemirgen',),),
        (RABBIT, _('Tavşan',),),
    )

    publisher_profile = models.ForeignKey('account.UserProfile', on_delete=models.CASCADE, related_name='adopt_posts')
    title = models.CharField(_(u'Başlık'), blank=False, null=False, max_length=40,)
    description = models.CharField(_(u'Açıklama'), blank=False, null=False, max_length=300,)
    image = models.TextField(_(u'Resim'), blank=True, null=True)
    pet_type = models.SmallIntegerField(_(u'Tür'), default=MALE, choices=TYPE_CHOICES)
    pet_breed = models.IntegerField(_(u'Irk'))
    pet_gender = models.SmallIntegerField(_(u'Cinsiyet'), default=MALE, choices=GENDER_CHOICES)
    country_code = models.CharField(_(u'Ülke Kodu'), blank=False, null=False, max_length=10,)
    city_name = models.CharField(_(u'Şehir'), blank=False, null=False, max_length=100,)
    district_name = models.CharField(_(u'İlçe'), blank=False, null=False,default="-", max_length=100)
    is_active = models.BooleanField(_(u'Durum'), default=True, blank=False, null=False)

    def __str__(self):
        return "{}-{}".format(self.id, self.title)
