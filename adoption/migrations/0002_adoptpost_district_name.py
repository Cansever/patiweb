# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2022-07-30 10:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adoption', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='adoptpost',
            name='district_name',
            field=models.CharField(default='-', max_length=100, verbose_name='İlçe'),
        ),
    ]
