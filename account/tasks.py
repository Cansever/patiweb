from __future__ import absolute_import, unicode_literals
from datetime import datetime
from celery import shared_task

from pyfcm import FCMNotification
from account.models import UserProfile
from matching.models import PetAlarm
from paticare.celery import app


@app.task()
def send_alarm():
    push_service = FCMNotification(
                        api_key="AAAA03lc-cQ:APA91bHzlTXiZPCeM308ANRcJYfWv8HhLxoZ5TNHj688eqZvkgKTotaqWXdeBWwA3szaKMF8e5L46FQn5qz4XApV1K3gy70wmJl9NIkBOghqMvv4q5xgr58ymCFOvUcwPuqxiLeytg-f")   

    alarms = PetAlarm.objects.filter(isActive=True, days__icontains=str(datetime.now().weekday()+1),time=datetime.now().strftime('%H:%M'))
    for alarm in alarms:
        result = push_service.notify_single_device(registration_id=alarm.pet_profile.user_profile.mobile_token,
                                                                  sound="default",
                                                                  message_title=alarm.pet_profile.pet_name,
                                                                  message_body=alarm.description)
    