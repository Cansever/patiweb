from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
from core.models import BaseModel
from django.utils import timezone

# Create your models here.


class UserProfile(BaseModel):

    UNKNOWN = 0
    MALE = 1
    FEMALE = 2

    GENDER_CHOICES = (
        (UNKNOWN, _('Bilinmiyor', ),),
        (MALE, _('Erkek',),),
        (FEMALE, _('Kadın',),),
    )

    user = models.ForeignKey(User, related_name='user_profiles', on_delete=models.CASCADE,)
    user_image = models.TextField(_(u'Profil Resmi'), blank=True, null=True)
    username = models.CharField(_(u'Kullanıcı Adı'), blank=False, unique=True, max_length=25)
    name = models.CharField(_(u'Kullanıcı Adı'), blank=False, max_length=25, default='')
    about_me = models.CharField(_(u'Hakkımda'), blank=True, null=True, max_length=300, default='')
    gender = models.SmallIntegerField(_(u'Cinsiyet'), default=UNKNOWN, choices=GENDER_CHOICES)
    birthdate = models.DateField(_(u'Age'), default=timezone.now)
    mobile_token = models.CharField(blank=True, null=True, max_length=250)
    login_type = models.CharField(blank=True, null=True, max_length=100)
    country_code = models.CharField(_(u'Ülke Kodu'), blank=False, null=False, max_length=10, default="TR")
    city_name = models.CharField(_(u'Şehir'), blank=True, null=True, max_length=100,)
    district_name = models.CharField(_(u'İlçe'), blank=True, null=True, max_length=100,)
    is_online = models.BooleanField(_(u'Durum'), default=True, blank=False, null=False)
    is_searchable = models.BooleanField(_(u'Pet Eşleştirme Durumu'), default=False, blank=False, null=False)

    def get_capitalize_name(self):
        return "{}".format(self.name.capitalize())

    def __str__(self):
        return "{}-{}".format(self.id, self.username)
