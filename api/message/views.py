from datetime import datetime, timedelta
import json
from unicodedata import category
from certifi import contents
from django.db import transaction
from django.dispatch import receiver
from django.http import Http404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from account.models import UserProfile
from api.message.serializers import ChatPanelSerializer, CreateMessageSerializer, MessageSerializer
from api.models import ChatPanel, Message
from django.db.models import Q
from pyfcm import FCMNotification

class ChatPanelListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ChatPanelSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = ChatPanel.objects.filter(
            Q(sender__user=self.request.user, sender_is_deleted=False)|
            Q(receiver__user=self.request.user, receiver_is_deleted=False)

        ).order_by("-updated_at")
 
        return queryset


class MessageListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = MessageSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = ChatPanel.objects.get(id=self.kwargs.get("id")).messages.all().order_by("created_at")
        return queryset


class ProfileMessageListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = MessageSerializer

    @transaction.atomic
    def get_queryset(self):
        profile_id = self.kwargs.get("profile_id")
        queryset = Message.objects.filter(
                Q(Q(sender_profile__id=profile_id) & Q(receiver_profile__user=self.request.user)) |
                Q(Q(sender_profile__user=self.request.user) & Q(receiver_profile__id=profile_id))
        ).order_by("created_at")
        return queryset


class CreateMessageView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateMessageSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
            
        if serializer.is_valid(raise_exception=True):
            receiver_profile = UserProfile.objects.get(id=serializer.validated_data['receiver_profile']["id"])
            sender_profile = UserProfile.objects.get(user=self.request.user)
            chat_id = None
            if int(self.kwargs.get("id")) == 0:
                q_set = ChatPanel.objects.filter(
                    Q(Q(sender=sender_profile) & Q(receiver=receiver_profile)) |
                    Q(Q(sender=receiver_profile)& Q(receiver=sender_profile))
                )
                if q_set.exists():
                    chat= q_set.get()
                else:
                    chat = ChatPanel.objects.create(sender=sender_profile,receiver=receiver_profile)
                chat_id = chat.id
            else:
                chat_id = self.kwargs.get("id")
            message = Message.objects.create(
                chat_panel_id=chat_id,
                content=serializer.validated_data['content'],
                sender_profile=sender_profile,
                receiver_profile=receiver_profile,
            )
            message.chat_panel.last_content=serializer.validated_data['content']
            message.chat_panel.sender_is_deleted = False
            message.chat_panel.receiver_is_deleted = False
            message.chat_panel.save()
            push_service = FCMNotification(
                        api_key="AAAA03lc-cQ:APA91bHzlTXiZPCeM308ANRcJYfWv8HhLxoZ5TNHj688eqZvkgKTotaqWXdeBWwA3szaKMF8e5L46FQn5qz4XApV1K3gy70wmJl9NIkBOghqMvv4q5xgr58ymCFOvUcwPuqxiLeytg-f")
            result = push_service.notify_single_device(registration_id=receiver_profile.mobile_token,
                                                                   sound="default",
                                                                   message_title=receiver_profile.username,
                                                                   message_body=message.content
                                                                   )
          
            return Response(status=200)

        return Response(status=404)


class ChatInfoView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = ChatPanelSerializer

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        receiver_profile = UserProfile.objects.get(id=self.kwargs.get("receiver_id"))
        sender_profile = UserProfile.objects.get(user=self.request.user)
        chat = ChatPanel.objects.get( 
              Q(Q(sender=sender_profile) & Q(receiver=receiver_profile)) |
              Q(Q(sender=receiver_profile)& Q(receiver=sender_profile))
        )
        
        return Response({
            "id":chat.id,
            "sender": {
                'token': chat.sender.user.auth_token.key,
                'id': chat.sender.pk,
                'login_type': chat.sender.login_type,
                'username': chat.sender.username,
                'name': chat.sender.get_capitalize_name(),
                'about_me': chat.sender.about_me,
                'gender': chat.sender.gender,
                'birthdate': chat.sender.birthdate if chat.sender.birthdate else None,
                'email': chat.sender.user.email,
                'mobile_token': chat.sender.mobile_token,
                'user_image': chat.sender.user_image,
                'country_code': chat.sender.country_code,
                'city_name': chat.sender.city_name,
                'is_online': 1 if chat.sender.is_online else 0,
                'is_searchable': 1 if chat.sender.is_searchable else 0,
            },
            "receiver":{
                'token': chat.receiver.user.auth_token.key,
                'id': chat.receiver.pk,
                'login_type': chat.receiver.login_type,
                'username': chat.receiver.username,
                'name': chat.receiver.get_capitalize_name(),
                'about_me': chat.receiver.about_me,
                'gender': chat.receiver.gender,
                'birthdate': chat.receiver.birthdate if chat.receiver.birthdate else None,
                'email': chat.receiver.user.email,
                'mobile_token': chat.receiver.mobile_token,
                'user_image': chat.receiver.user_image,
                'country_code': chat.receiver.country_code,
                'city_name': chat.receiver.city_name,
                'is_online': 1 if chat.receiver.is_online else 0,
                'is_searchable': 1 if chat.receiver.is_searchable else 0,
            },
            "last_content":chat.last_content
        })


class DeleteChatPanelView(APIView):
    permission_classes = (AllowAny,)

    def get_object(self, id):
        try:
            return ChatPanel.objects.get(id=id)
        except ChatPanel.DoesNotExist:
            raise Http404

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        chat = self.get_object(self.kwargs.get("id"))
        user_profile = UserProfile.objects.get(user=self.request.user)
        if chat.sender == user_profile:
            chat.sender_is_deleted = True
        else:
            chat.receiver_is_deleted = True
        if chat.sender_is_deleted == True and chat.receiver_is_deleted == True:
            chat.delete()
        else:
            chat.save()

        return Response(status=200)
