import datetime
from rest_framework import serializers
from api.account.serializers import UserProfileSerializer


class ChatPanelSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    sender = UserProfileSerializer()
    receiver = UserProfileSerializer()    
    last_content = serializers.CharField(allow_blank=True, allow_null=True)
    updated_at = serializers.SerializerMethodField(read_only=True)

    def get_updated_at(self, obj):
        return (obj.updated_at + datetime.timedelta(hours=3)).strftime('%Y-%m-%d %H:%M:%S')

class CreateMessageSerializer(serializers.Serializer):
    content = serializers.CharField(allow_null=False, allow_blank=False) 
    receiver_profile = UserProfileSerializer()

class MessageSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    sender_profile = UserProfileSerializer()
    receiver_profile = UserProfileSerializer()    
    content = serializers.CharField(allow_blank=False, allow_null=False)
    created_at = serializers.SerializerMethodField(read_only=True)

    def get_created_at(self, obj):  
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%H:%M')
