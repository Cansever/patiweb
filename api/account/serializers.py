import datetime

from rest_framework import serializers

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    login_type = serializers.CharField()
    mobile_token = serializers.CharField()

class UserProfileSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    user_image = serializers.CharField(allow_null=True)
    username = serializers.CharField()
    name = serializers.CharField()
    about_me = serializers.CharField(allow_null=True, allow_blank=True)
    gender = serializers.IntegerField(default=0)
    birthdate = serializers.SerializerMethodField(allow_null=True)
    country_code = serializers.CharField()
    city_name = serializers.CharField(allow_null=True, allow_blank=True)
    district_name = serializers.CharField(allow_null=True, allow_blank=True)
    is_online = serializers.SerializerMethodField()
    is_searchable = serializers.SerializerMethodField()

    def get_birthdate(self, obj):
        return "" #(obj.birthdate + datetime.timedelta(hours=3)).strftime('%Y-%m-%d')

    def get_is_online(self, obj):
        return 1 if obj.is_online else 0

    def get_is_searchable(self, obj):
        return 1 if obj.is_searchable else 0


class ProfilePasswordSerializer(serializers.Serializer):
    old_pass = serializers.CharField()
    new_pass = serializers.CharField()


class SignUpSerializer(serializers.Serializer):
    username = serializers.CharField()
    name = serializers.CharField()
    email = serializers.CharField()
    password = serializers.CharField()
    mobile_token = serializers.CharField()
    login_type = serializers.CharField()
    country_code = serializers.CharField()


class PasswordResetSerializer(serializers.Serializer):
    username = serializers.CharField()
    profile_type = serializers.CharField()


class PetProfileSerializer(serializers.Serializer):

    id = serializers.IntegerField(allow_null=True)
    user_profile = UserProfileSerializer()
    pet_profile_image = serializers.CharField(allow_null=True)
    pet_name = serializers.CharField()
    description = serializers.CharField(allow_null=True, allow_blank=True)
    pet_gender = serializers.IntegerField()
    pet_breed = serializers.IntegerField()
    pet_type = serializers.IntegerField()
    #pet_age = serializers.IntegerField()
    is_matching = serializers.SerializerMethodField()
    updated_at = serializers.SerializerMethodField(read_only=True)
    pet_birthdate = serializers.SerializerMethodField(allow_null=True)

    def get_is_matching(self, obj):
        return 1 if obj.is_matching else 0

    def get_updated_at(self, obj):
        return (obj.updated_at + datetime.timedelta(hours=3)).strftime('%d/%m/%Y')

    def get_pet_birthdate(self, obj):
        return (obj.pet_birthdate + datetime.timedelta(hours=3)).strftime('%Y-%m-%d')
    #pet_birthdate = serializers.CharField(allow_null=True)
    # country_code = serializers.CharField()
    # city_name = serializers.CharField(allow_null=True)
    # is_online = serializers.SerializerMethodField()
    # is_searchable = serializers.SerializerMethodField()
    #
    # def get_is_online(self, obj):
    #     return 1 if obj.is_online else 0
    #
    # def get_is_searchable(self, obj):
    #     return 1 if obj.is_searchable else 0

class PetWeightSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    created_at = serializers.SerializerMethodField()
    weight = serializers.FloatField()

    def get_created_at(self, obj):
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%Y-%m-%d %H:%M')

class PetCreateWeightSerializer(serializers.Serializer):
    weight = serializers.FloatField()

class PetAlarmSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    created_at = serializers.SerializerMethodField()
    pet_profile = PetProfileSerializer()
    category = serializers.IntegerField()
    days = serializers.ListField()
    time = serializers.CharField()
    description = serializers.CharField()
    isActive = serializers.BooleanField()

    def get_created_at(self, obj):
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%Y-%m-%d %H:%M')
