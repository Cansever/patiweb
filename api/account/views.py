from pickle import NONE
import random
import uuid
from datetime import date, datetime
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import Count
from django.contrib.auth import authenticate, logout
from django.http import Http404
from rest_framework import status, generics

from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import APIException

from account.models import UserProfile
from api.account.serializers import LoginSerializer, PetAlarmSerializer, PetCreateWeightSerializer, PetProfileSerializer, PetWeightSerializer, SignUpSerializer, ProfilePasswordSerializer, \
    UserProfileSerializer
from api.feed.serializers import QuestionSerializer
from core.views import ErrorAPICode
from feed.models import Question
from matching.models import PetAlarm, PetProfile, PetWeight


class LoginView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        username = serializer.validated_data['username']
        password = serializer.validated_data['password']
        mobileToken = serializer.validated_data['mobile_token']

        profiles = None

        if str(username).find("@") == -1: #username girişi
            profiles = UserProfile.objects.filter(username=username)
            if profiles.exists():
                profiles = profiles
            else:
                return Response(ErrorAPICode.WRONG_USERNAME, status=status.HTTP_401_UNAUTHORIZED)

             #   raise AuthenticationFailed(
              #      _('Giriş yapmaya çalıştığınız hesabın ilişkili kullanıcı bilgisi bulunamadı.'))

        else: # email ile giriş
            if UserProfile.objects.filter(user__email=username).exists():
                profiles = UserProfile.objects.filter(user__email=username)
            else:
                return Response(ErrorAPICode.WRONG_EMAIL, status=status.HTTP_401_UNAUTHORIZED)
               # raise AuthenticationFailed(
                #    _('Giriş yapmaya çalıştığınız hesabın ilişkili email bilgisi bulunamadı.'))

        if not profiles:
            return Response(ErrorAPICode.INVALID_AUTH, status=status.HTTP_401_UNAUTHORIZED)

        profile = profiles[0]

        if not Token.objects.filter(user=profile.user).exists():
            Token.objects.create(user=profile.user)

        if not check_password(password, profile.user.password):
            return Response(ErrorAPICode.INVALID_PASS, status=status.HTTP_401_UNAUTHORIZED)

        user = None
       # if profile.login_type != "facebook" and profile.login_type != "gmail":
        if not profile.user.is_authenticated():
            user = authenticate(username=profile.user.username, password=password)
        #else:
        #    if not profile.user.is_authenticated():
        #        user = authenticate(username=profile.user.username, password=profile.mobile_token[:50])
        if profile.user.is_authenticated():
            user = profile.user
        if not user:
            return Response(ErrorAPICode.INVALID_AUTH, status=status.HTTP_401_UNAUTHORIZED)
        profile.mobile_token = mobileToken
        profile.is_online = True
        profile.user.save()
        profile.save()
        return Response(
            {
                'token': profile.user.auth_token.key,
                'id': profile.pk,
                'login_type': profile.login_type,
                'username': profile.username,
                'name': profile.get_capitalize_name(),
                'about_me': profile.about_me,
                'gender': profile.gender,
                'birthdate': "", #profile.birthdate if profile.birthdate else None,
                'email': profile.user.email,
                'mobile_token': mobileToken,
                'user_image': profile.user_image,
                'country_code': profile.country_code,
                'city_name': profile.city_name,
                'district_name': profile.district_name if profile.district_name != None else "",
                'is_online': 1 if profile.is_online else 0,
                'is_searchable': 1 if profile.is_searchable else 0,
            }
        )


class SignUpView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        username = serializer.validated_data['username']
        name = serializer.validated_data['name']
        email = serializer.validated_data['email']
        password = serializer.validated_data['password']
        mobileToken = serializer.validated_data['mobile_token']
        login_type = serializer.validated_data['login_type']
        country_code = serializer.validated_data['country_code']

        user  = None
        if login_type != "gmail" and login_type != "facebook": #social media girişi değilse
            if UserProfile.objects.filter(username=username).exists():
                    return Response(ErrorAPICode.ALREADY_EXIST_USERNAME, status=status.HTTP_401_UNAUTHORIZED)

            if UserProfile.objects.filter(user__email=email).exclude(login_type="temp", user__email="anonymous_temp@mail.com").exists():
                return Response(ErrorAPICode.ALREADY_EXIST_EMAIL, status=status.HTTP_401_UNAUTHORIZED)

            if UserProfile.objects.filter(username=username, user__email=email).exists():
                return Response(ErrorAPICode.ALREADY_EXIST_ACCOUNT, status=status.HTTP_401_UNAUTHORIZED)
            else:
                if login_type == "temp":
                    username = "{}".format(uuid.uuid1())[:20]
                    user = User.objects.create(username=username, email=email)
                else:
                    user = User.objects.create(username=str(uuid.uuid1()), email=email)

                Token.objects.create(user=user)

            profile = UserProfile.objects.create(user=user, username=username, name=name, mobile_token=mobileToken, login_type=login_type,country_code=country_code)
            profile.user.set_password(password)
            profile.user.save()

        else:#social media girişiyse
            if UserProfile.objects.filter(user__email=email, login_type=login_type).exists():
                profile = UserProfile.objects.get(user__email=email)
                profile.mobile_token = mobileToken
                if not Token.objects.filter(user=profile.user):
                    Token.objects.create(user=profile.user)
                profile.save()
            else:#yoksa kullanıcı oluştur
                if login_type == "facebook" or login_type == "gmail":
                    random_number = str(random.randint(0,1000)) + str(random.randint(0,1000))
                    username = "{}_{}".format(random_number, username)
                user = User.objects.create(username=username, email=email)
                Token.objects.create(user=user)
                profile = UserProfile.objects.create(user=user, mobile_token=mobileToken, login_type=login_type)
                profile.user.set_password(password)
                profile.user.save()

        return Response({
            'token': profile.user.auth_token.key,
            'id': profile.pk,
            'username': profile.username,
            'mobile_token': mobileToken
        })


class ProfileInfoView(APIView):
    permission_classes = (AllowAny,)

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        user_profile = UserProfile.objects.get(id=self.kwargs['user_id'])
        return Response({
                'id': user_profile.pk,
                'login_type': user_profile.login_type,
                'username': user_profile.username,
                'name': user_profile.get_capitalize_name(),
                'about_me': user_profile.about_me,
                'gender': user_profile.gender,
                'birthdate': "",#user_profile.birthdate.strftime('%d-%m-%Y') if user_profile.birthdate else None,
                'email': user_profile.user.email,
                'mobile_token': "",
                'user_image': user_profile.user_image,
                'country_code': user_profile.country_code,
                'city_name': user_profile.city_name,
                'district_name': user_profile.district_name if  user_profile.district_name != None else "",
                'is_online': 1 if user_profile.is_online else 0,
                'is_searchable': 1 if user_profile.is_searchable else 0,
        })


class UpdateUserProfileView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserProfileSerializer
    model = UserProfile

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = UserProfileSerializer(data=request.data)

        profile = UserProfile.objects.get(id=self.kwargs['user_id'])
        if serializer.is_valid(raise_exception=True):
            if profile.username != serializer.validated_data["username"]:
                profiles = UserProfile.objects.filter(username=serializer.validated_data["username"])
                if profiles.exists():
                    return Response(ErrorAPICode.ALREADY_EXIST_USERNAME, status=status.HTTP_400_BAD_REQUEST)
                else:
                    profile.username = serializer.validated_data["username"]

            profile.user_image = serializer.validated_data['user_image']
            profile.name = serializer.validated_data['name']
            profile.gender = serializer.validated_data['gender']
            profile.city_name = serializer.validated_data['city_name']
            profile.district_name = serializer.validated_data['district_name']
           ## date = serializer.validated_data['birthdate']
           ## profile.birthdate = datetime.strptime(date,"%d-%m-%Y").date()
            profile.country_code = serializer.validated_data["country_code"]
            profile.save()
            return Response(status=200)

        return Response(status=404)

class ChangePasswordProfileView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = ProfilePasswordSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = ProfilePasswordSerializer(data=request.data)

        profile = UserProfile.objects.get(id=self.kwargs['user_id'])

        if serializer.is_valid(raise_exception=True):
            old_pass = serializer.validated_data['old_pass']
            new_pass = serializer.validated_data['new_pass']
            if not check_password(old_pass, profile.user.password):
                return Response(ErrorAPICode.INVALID_PASS, status=status.HTTP_401_UNAUTHORIZED)
            profile.is_online = False
            profile.save()
            profile.user.set_password(new_pass)
            profile.user.save()

            return Response(status=200)

        return Response(status=404)


class UserMobileTokenDelete(APIView):
    permission_classes = (AllowAny,)

    def get_object(self, token):
        try:
            return UserProfile.objects.get(id=self.kwargs['user_id'], mobile_token=token)
        except UserProfile.DoesNotExist:
            raise APIException("User not found")

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        profile = self.get_object(self.request.data.get("mobile_token"))
        profile.mobile_token = ""
        logout(request)
        Token.objects.filter(user=profile.user).delete()
        profile.user.is_authenticated.value = False
        profile.save()
        profile.user.save()
        return Response(status=200)


class CreateUpdatePetProfileView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = PetProfileSerializer
    model = PetProfile

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = PetProfileSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
           user_profile = UserProfile.objects.get(id=self.kwargs['id'])
           PetProfile.objects.create(
               user_profile=user_profile,
               pet_profile_image = serializer.validated_data['pet_profile_image'],
               pet_name = serializer.validated_data['pet_name'],
               pet_gender = serializer.validated_data['pet_gender'],
               pet_breed = serializer.validated_data['pet_breed'],
               pet_type = serializer.validated_data['pet_type'],
               pet_birthdate = self.request.data["pet_birthdate"]
               )

           return Response(status=200)

        return Response(status=404)

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        serializer = PetProfileSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            pet_profile = PetProfile.objects.get(id=self.kwargs['id'])
            pet_profile.pet_profile_image = serializer.validated_data['pet_profile_image']
            pet_profile.pet_name = serializer.validated_data['pet_name']
            pet_profile.description = serializer.validated_data['description']
            pet_profile.pet_gender = serializer.validated_data['pet_gender']
            pet_profile.pet_breed = serializer.validated_data['pet_breed']
            pet_profile.pet_type = serializer.validated_data['pet_type']
            pet_profile.pet_birthdate = datetime.strptime(self.request.data["pet_birthdate"], "%Y-%m-%d").date()
            pet_profile.save()

            return Response(status=200)

        return Response(status=404)


class UpdatePetMatchingProfileView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = PetProfileSerializer
    model = PetProfile

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = PetProfileSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
           if self.request.GET.get("searchable") == "true":
                pet = PetProfile.objects.get(id=self.kwargs['pet_id'])
                pet.is_matching = True
                pet.description = serializer.validated_data['description'] if serializer.validated_data['description'] != "" else None
                pet.save()
           else:
                pet = PetProfile.objects.get(id=self.kwargs['pet_id'])
                pet.is_matching = False
                pet.save()
           return Response(status=200)

        return Response(status=404)


class ProfilePetListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PetProfileSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = PetProfile.objects.filter(user_profile_id=self.kwargs.get("id")).order_by("-created_at")
        return queryset


class ProfilePetWeightListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PetWeightSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = PetWeight.objects.filter(pet_profile_id=self.kwargs.get("id")).order_by("-created_at")
        return queryset

class CreatePetWeightView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = PetCreateWeightSerializer

    def get_object(self):
        try:
            return PetProfile.objects.get(id=self.kwargs.get("id"))
        except PetProfile.DoesNotExist:
            raise Http404

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        profile = self.get_object()
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            PetWeight.objects.create(weight=serializer.validated_data['weight'], pet_profile=profile)
            return Response(status=200)
        return Response(status=404)

class DeletePetWeightView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return PetWeight.objects.get(id=id)
        except PetWeight.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        weight = self.get_object(self.kwargs.get("id"))
        weight.delete()
        return Response(status=200)


class ProfilePetAlarmListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PetAlarmSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = PetAlarm.objects.filter(pet_profile_id=self.kwargs.get("id")).order_by("-created_at")
        return queryset

class ProfilePetAlarmTodayListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PetAlarmSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = PetAlarm.objects.filter(pet_profile__user=self.request.user, days__in=["0", str(datetime.today().weekday)])
        return queryset

class CreateUpdatePetAlarmView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = PetAlarmSerializer

    def get_object(self):
        try:
            return PetProfile.objects.get(id=self.kwargs.get("id"))
        except PetProfile.DoesNotExist:
            raise Http404

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        profile = self.get_object()
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            PetAlarm.objects.create(
                description=serializer.validated_data['description'],
                pet_profile=profile,
                category=serializer.validated_data['category'],
                days=serializer.validated_data['days'],
                time=serializer.validated_data['time'],
                isActive=serializer.validated_data['isActive'],
            )
            return Response(status=200)
        return Response(status=404)

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        profile = self.get_object()
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            alarm = PetAlarm.objects.get(pet_profile=profile,id=serializer.validated_data['id'])
            alarm.description=serializer.validated_data['description']
            alarm.category=serializer.validated_data['category']
            alarm.days=serializer.validated_data['days']
            alarm.time=serializer.validated_data['time']
            alarm.isActive=serializer.validated_data['isActive']
            alarm.save()
            return Response(status=200)
        return Response(status=404)

class DeletePetAlarmView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return PetAlarm.objects.get(id=id)
        except PetAlarm.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        alarm = self.get_object(self.kwargs.get("id"))
        alarm.delete()
        return Response(status=200)


class DeletePetView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return PetProfile.objects.get(id=id)
        except PetProfile.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        pet = self.get_object(self.kwargs.get("id"))
        pet.delete()
        return Response(status=200)
