from random import shuffle

from django.db import transaction
from django.db.models import Q
from django.http import Http404
from rest_framework import generics
from rest_framework.exceptions import APIException
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from account.models import UserProfile
from api.account.serializers import PetProfileSerializer
from matching.models import MatchingLike, PetProfile
from rest_framework.response import Response


class MatchingListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PetProfileSerializer

    @transaction.atomic
    def get_queryset(self):
        pet_type = self.kwargs['type']
        country_code = self.kwargs['country_code']
        pet_breed = self.kwargs['breed']
        pet_gender = self.kwargs['gender']
        city_name = self.kwargs['city'].replace("-", " ",self.kwargs['city'].count('-'))
        district_name = self.kwargs['district'].replace("-", " ",self.kwargs['district'].count('-'))
        filters = {}
        if pet_type != "-1":
            filters["pet_type"] = pet_type
        filters["is_matching"] = True
        filters["user_profile__country_code"] = country_code
        if pet_breed != "-1":
            filters["pet_breed"] = pet_breed
        if pet_gender != "-1":
            filters["pet_gender"] = pet_gender
        if city_name != "all" and city_name != "Tümü":
            filters["user_profile__city_name"] = city_name
        if district_name != "all" and district_name != "Tümü":
            filters["user_profile__district_name"] = district_name
        queryset = PetProfile.objects.filter(**filters)
        return queryset
