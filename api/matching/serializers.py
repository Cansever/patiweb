import datetime

from rest_framework import serializers


class PetSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    user_image = serializers.CharField(allow_null=True)
    username = serializers.CharField()
    name = serializers.CharField()
    about_me = serializers.CharField(allow_null=True, allow_blank=True)
    gender = serializers.IntegerField(default=0)
    city_name = serializers.CharField(allow_null=True, allow_blank=True)
    is_matching = serializers.SerializerMethodField()
 
    def get_is_matching(self, obj):
        return 1 if obj.is_matching else 0