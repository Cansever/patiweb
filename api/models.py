from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils.translation import ugettext as _

from core.models import BaseModel


class ReportInappropriate(BaseModel):

    ACCOUNT = 1
    QUESTION = 3
    ANSWER = 4

    TYPE_CHOICES = (
        (ACCOUNT, _('Hesap',),),
        (QUESTION, _('Soru',),),
        (ANSWER, _('Cevap', ),),
    )

    user = models.ForeignKey(User, related_name='user_report_inappropriates', on_delete=models.CASCADE,)
    type = models.SmallIntegerField(_(u'Tür'), default=ACCOUNT, choices=TYPE_CHOICES)
    dyn_model_id = models.IntegerField(_(u'Model id'))


class ChatPanel(BaseModel):
    sender = models.ForeignKey('account.UserProfile',null=True, blank=True, on_delete=models.CASCADE, related_name='sender_chatpanels',)
    receiver = models.ForeignKey('account.UserProfile',null=True, blank=True, on_delete=models.CASCADE, related_name='receiver_chatpanels',)
    last_content = models.TextField(null=True, blank=True)
    sender_is_deleted = models.BooleanField(default=False)
    receiver_is_deleted = models.BooleanField(default=False)



class Message(BaseModel):
    chat_panel = models.ForeignKey('api.ChatPanel', on_delete=models.CASCADE, related_name='messages',)
    content = models.TextField()
    is_read = models.BooleanField(default=False)
    sender_profile = models.ForeignKey('account.UserProfile', on_delete=models.CASCADE, related_name='sender_messages',)
    receiver_profile = models.ForeignKey('account.UserProfile', on_delete=models.CASCADE, related_name='receiver_messages',)

