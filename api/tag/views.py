from urllib import response
from django.db import transaction
from django.db.models import Count
from django.http import Http404
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from api.tag.serializers import TagCreateSerializer, TagSerializer, TagListSerializer
from tag.models import Tag
from django.db.models import Q, Count, Case, When, IntegerField, Value
from core.views import ErrorAPICode

class TagListView(APIView):
    permission_classes = (AllowAny,)

    @transaction.atomic
    def get(self, request):
        queryset = Tag.objects.all()
        data = []
        for i in queryset:
            freq = i.get_tag_used_count()
            if freq > 0:
                data.append({
                    'id': i.id,
                    'name': i.name.lower(),
                    'freq': str(freq)
                })
        sorted_list = sorted(data, key=lambda x:x['freq'],reverse=True)
        return Response(sorted_list)


class TagSearchListView(generics.ListAPIView):
    permission_classes = (AllowAny,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = TagListSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        queryset = Tag.objects.filter(name__icontains=data["name"]).order_by("name")[:5]
        data = []
        for i in queryset:
            data.append({
                'id': i.id,
                'name': i.name.lower(),
                'freq': str(i.get_tag_used_count())
            })
        return Response(data)


class CreateTagView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = TagCreateSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            if not Tag.objects.filter(name=serializer.validated_data['name']).exists():
                tag = Tag.objects.create(
                    name=serializer.validated_data['name'],
                )
                return Response({
                'id': tag.id,
                'name': tag.name.lower(),
            })
            return  Response(ErrorAPICode.ALREADY_EXIST_TAG, status=status.HTTP_406_NOT_ACCEPTABLE)

        return Response(status=404)


class TagInfoView(APIView):
    permission_classes = (AllowAny,)

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        tag = Tag.objects.get(Q(id=self.kwargs.get("id")))
        return Response(
            {
                "id":tag.id,
                "name":tag.name
            }
        )


class DeleteTagView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return Tag.objects.get(id=id)
        except Tag.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        tag = self.get_object(self.kwargs.get("id"))
        tag.delete()
        return Response(status=200)