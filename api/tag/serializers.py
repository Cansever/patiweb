from rest_framework import serializers

class TagSerializer(serializers.Serializer):
    id = serializers.IntegerField() 
    name = serializers.CharField(allow_null=False, allow_blank=False) 

class TagCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=False, allow_blank=False) 
  
class TagListSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=False, allow_blank=False)   
    freq = serializers.CharField(allow_null=True, allow_blank=True)