from django.db import transaction
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import ReportInappropriate
from feed.models import Question, Answer


class SendReportInappropriateView(APIView):
    permission_classes = (AllowAny,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        id = int(self.kwargs['id'])
        if ReportInappropriate.QUESTION == int(self.kwargs['type']):
            reports = ReportInappropriate.objects.filter(user=self.request.user, type=self.kwargs['type'], dyn_model_id=id)
        elif ReportInappropriate.ANSWER == int(self.kwargs['type']):
            reports = ReportInappropriate.objects.filter(user=self.request.user, type=self.kwargs['type'], dyn_model_id=id)
        elif ReportInappropriate.ACCOUNT == int(self.kwargs['type']):
            reports = ReportInappropriate.objects.filter(user=self.request.user, type=self.kwargs['type'], dyn_model_id=id)

        if reports.exists():
            ReportInappropriate.objects.create(user=self.request.user, type=self.kwargs['type'], dyn_model_id=id)
            #silindiğinde ilgili kişiye bildirim gönderilebilir
            reports = ReportInappropriate.objects.filter(type=self.kwargs['type'], dyn_model_id=id)
            if reports.count() > 9 and (ReportInappropriate.QUESTION == int(self.kwargs['type']) or ReportInappropriate.ANSWER == int(self.kwargs['type'])):
                if ReportInappropriate.QUESTION == int(self.kwargs['type']):
                    Question.objects.filter(id=id).delete()
                elif ReportInappropriate.ANSWER == int(self.kwargs['type']):
                    Answer.objects.filter(id=id).delete()

        return Response(status=200)
