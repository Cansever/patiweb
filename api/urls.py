from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from api.account import views as account_views
from api.feed import views as feed_views
from api.adoption import views as adopt_views
from api.matching import views as matching_views
from api.tag import views as tag_views
from api.message import views as message_views
from api import views as api_views
from core import views as core_views

router = routers.DefaultRouter()
urlpatterns = [
    url('api-token-auth/', obtain_auth_token, name='api_token_auth'),
    url(r'^account/login/$', account_views.LoginView.as_view()),
    url(r'^account/signup/$', account_views.SignUpView.as_view()),
    url(r'^account/user_profile_info/(?P<user_id>\d+)/$', account_views.ProfileInfoView.as_view()),
    url(r'^account/update_profile/(?P<user_id>\d+)/$', account_views.UpdateUserProfileView.as_view()),
    url(r'^account/change_password/(?P<user_id>\d+)/$', account_views.ChangePasswordProfileView.as_view()),
    url(r'^token_delete/(?P<user_id>\d+)/$',account_views.UserMobileTokenDelete.as_view()),

    url(r'^account/create_update_pet_profile/(?P<id>\d+)/$', account_views.CreateUpdatePetProfileView.as_view()),
    url(r'^account/update_pet_matching_profile/(?P<pet_id>\d+)/$', account_views.UpdatePetMatchingProfileView.as_view()),
    url(r'^account/pet_list/(?P<id>\d+)/$', account_views.ProfilePetListView.as_view()),
    url(r'^account/delete_question/(?P<id>\d+)/$', account_views.DeletePetView.as_view()),
    url(r'^account/pet_weight_list/(?P<id>\d+)/$', account_views.ProfilePetWeightListView.as_view()),
    url(r'^account/pet_weight_create/(?P<id>\d+)/$', account_views.CreatePetWeightView.as_view()),
    url(r'^account/delete_weight/(?P<id>\d+)/$', account_views.DeletePetWeightView.as_view()),
    url(r'^account/pet_alarm_list/(?P<id>\d+)/$', account_views.ProfilePetAlarmListView.as_view()),
    url(r'^account/pet_alarm_create_update/(?P<id>\d+)/$', account_views.CreateUpdatePetAlarmView.as_view()),
    url(r'^account/delete_alarm/(?P<id>\d+)/$', account_views.DeletePetAlarmView.as_view()),
    url(r'^account/get_today_alarm_list/$', account_views.ProfilePetAlarmTodayListView.as_view()),


    url(r'^feed/question_list/(?P<time_filter>[\w\-]+)/(?P<filterTagId>[\w\-]+)/(?P<take_data_count>\d+)/$', feed_views.QuestionListView.as_view()),
    url(r'^feed/answer_list/(?P<id>\d+)/$', feed_views.QuestionAnswerListView.as_view()),
    url(r'^feed/new_question/$', feed_views.CreateQuestionView.as_view()),
    url(r'^feed/create_answer/(?P<id>\d+)/$', feed_views.CreateAnswerView.as_view()),
    url(r'^feed/delete_question/(?P<id>\d+)/$', feed_views.DeleteQuestionView.as_view()),
    url(r'^feed/delete_answer/(?P<id>\d+)/$', feed_views.DeleteAnswerView.as_view()),

    url(r'^report/(?P<type>\d+)/(?P<id>\d+)/$', api_views.SendReportInappropriateView.as_view()),

    url(r'^adoption/adopt_list/(?P<country_code>[\w\-]+)/(?P<type>[\w\-]+)/(?P<pet_type>[\w\-]+)/filters/(?P<breed>[\w\-]+)/(?P<gender>[\w\-]+)/(?P<city>[\w\-]+)/(?P<district>[\w\-]+)/(?P<take_data_count>\d+)/$', adopt_views.AdoptPostListView.as_view()),
    url(r'^adoption/new_adopt/$', adopt_views.CreateAdoptPostView.as_view()),
    url(r'^adoption/update_adopt/(?P<id>\d+)/$', adopt_views.UpdateAdoptPostView.as_view()),
    url(r'^adoption/delete_post/(?P<id>\d+)/$', adopt_views.DeletePostView.as_view()),

    url(r'^matching/matching_list/(?P<country_code>[\w\-]+)/filters/(?P<type>[\w\-]+)/(?P<breed>[\w\-]+)/(?P<gender>[\w\-]+)/(?P<city>[\w\-]+)/(?P<district>[\w\-]+)/$', matching_views.MatchingListView.as_view()),

    url(r'^tag/tag_list/$', tag_views.TagListView.as_view()),
    url(r'^tag/search_tag_list/$',tag_views.TagSearchListView.as_view()),
    url(r'^tag/new_tag/$', tag_views.CreateTagView.as_view()),
    url(r'^tag/tag_info/(?P<id>\d+)/$', tag_views.TagInfoView.as_view()),
    url(r'^tag/delete_tag/(?P<id>\d+)/$', tag_views.DeleteTagView.as_view()),


    url(r'^message/chatpanel_list/$', message_views.ChatPanelListView.as_view()),
    url(r'^message/message_list/(?P<id>\d+)/$', message_views.MessageListView.as_view()),
    url(r'^message/message_list_with_profile/(?P<profile_id>\d+)/$', message_views.ProfileMessageListView.as_view()),
    url(r'^message/new_message/(?P<id>\d+)/$', message_views.CreateMessageView.as_view()),
    url(r'^message/chat_info/(?P<receiver_id>\d+)/$', message_views.ChatInfoView.as_view()),
    url(r'^message/delete_message/(?P<id>\d+)/$', message_views.DeleteChatPanelView.as_view()),

    url(r'^core/activity/(?P<id>\d+)/(?P<activity_type>[\w\-]+)/(?P<category_type>[\w\-]+)/$',core_views.ActivityView.as_view()),

]
