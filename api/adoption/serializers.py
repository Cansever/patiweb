import datetime

from rest_framework import serializers

from api.account.serializers import UserProfileSerializer


class AdoptPostSerializer(serializers.Serializer):
    publisher_profile = UserProfileSerializer()
    created_at = serializers.SerializerMethodField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField()
    description = serializers.CharField()
    image = serializers.CharField(allow_null=True)
    pet_gender = serializers.IntegerField()
    pet_breed = serializers.IntegerField()
    pet_type = serializers.IntegerField()
    country_code = serializers.CharField()
    city_name = serializers.CharField()
    district_name = serializers.CharField()
    is_active = serializers.BooleanField(default=False)

    def get_created_at(self, obj):
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%d-%m-%Y %H:%M:%S')


class CreateUpdateAdoptPostSerializer(serializers.Serializer):
    title = serializers.CharField()
    description = serializers.CharField()
    image = serializers.CharField(allow_null=True)
    pet_gender = serializers.IntegerField()
    pet_breed = serializers.IntegerField()
    pet_type = serializers.IntegerField()
    country_code = serializers.CharField()
    city_name = serializers.CharField()
    district_name = serializers.CharField()
