from django.db import transaction
from django.http import Http404
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime

from account.models import UserProfile
from adoption.models import AdoptPost
from api.adoption.serializers import AdoptPostSerializer, CreateUpdateAdoptPostSerializer


class AdoptPostListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = AdoptPostSerializer

    @transaction.atomic
    def get_queryset(self):
        filter_type = self.kwargs['type']
        country_code = self.kwargs['country_code']
        start_index = int(self.kwargs['take_data_count'])
        pet_breed = self.kwargs['breed']
        pet_type = self.kwargs['pet_type']
        pet_gender = self.kwargs['gender']
        city_name = self.kwargs['city']
        district_name = self.kwargs['district']

        next_data_count = 35

        if filter_type == "me": #my_posts
            queryset = AdoptPost.objects.filter(publisher_profile__user=self.request.user).order_by("-created_at")
        else: #posts
            if pet_type != "-1":
                queryset = AdoptPost.objects.filter(country_code=country_code,pet_type=pet_type).order_by("-created_at")
            else:
                queryset = AdoptPost.objects.filter(country_code=country_code).order_by("-created_at")
      #  else: #dog :2
       #     queryset = AdoptPost.objects.filter(country_code=country_code,pet_type=filter_type)

        filters = {}
        if filter_type != "me":
            if pet_breed != "-1":
                filters["pet_breed"] = pet_breed.replace("-", " ",pet_breed.count('-'))
            if pet_gender != "-1":
                filters["pet_gender"] = pet_gender
            if city_name != "all" and city_name != "Tümü":
                filters["city_name"] = city_name.replace("-", " ",city_name.count('-'))
            if district_name != "all" and district_name != "Tümü":
                filters["district_name"] = district_name.replace("-", " ",district_name.count('-'))
            queryset = queryset.filter(**filters)

        if queryset.count() > start_index:
            queryset = queryset[start_index:(start_index + next_data_count)]
        else:
            queryset = queryset[start_index + 1:(start_index + next_data_count)]

        return queryset


class CreateAdoptPostView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateUpdateAdoptPostSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            profile = UserProfile.objects.get(user=self.request.user)
            image = serializer.validated_data['image']
            title = serializer.validated_data['title']
            description = serializer.validated_data['description']
            pet_type = serializer.validated_data['pet_type']
            pet_gender = serializer.validated_data['pet_gender']
            pet_breed = serializer.validated_data['pet_breed']
            country_code = serializer.validated_data['country_code']
            city_name = serializer.validated_data['city_name']
            district_name = serializer.validated_data['district_name']

            AdoptPost.objects.create(
                image=image,
                title=title,
                description=description,
                pet_type=pet_type,
                pet_gender=pet_gender,
                pet_breed=pet_breed,
                country_code=country_code,
                city_name=city_name,
                district_name=district_name,
                publisher_profile=profile,
            )

            return Response(status=200)

        return Response(status=404)


class UpdateAdoptPostView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateUpdateAdoptPostSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            post_id = int(self.kwargs['id'])
            post = AdoptPost.objects.get(id=post_id)
            image = serializer.validated_data['image']
            title = serializer.validated_data['title']
            description = serializer.validated_data['description']
            pet_type= serializer.validated_data['pet_type']
            pet_gender = serializer.validated_data['pet_gender']
            pet_breed = serializer.validated_data['pet_breed']
            country_code = serializer.validated_data['country_code']
            city_name = serializer.validated_data['city_name']
            district_name = serializer.validated_data['district_name']

            post.image = image
            post.title = title
            post.description = description
            post.pet_type =pet_type
            post.pet_gender = pet_gender
            post.pet_breed = pet_breed
            post.country_code = country_code
            post.city_name = city_name
            post.district_name = district_name
            post.save()
            return Response(status=200)

        return Response(status=404)


class DeletePostView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return AdoptPost.objects.get(id=id)
        except AdoptPost.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        post = self.get_object(self.kwargs.get("id"))
        post.delete()
        return Response(status=200)
