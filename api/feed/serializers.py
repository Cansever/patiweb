import datetime

from rest_framework import serializers

from api.account.serializers import UserProfileSerializer


class QuestionSerializer(serializers.Serializer):
    user_profile = UserProfileSerializer()
    created_at = serializers.SerializerMethodField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    description = serializers.CharField(allow_blank=True, allow_null=True)
    is_like = serializers.BooleanField(default=False)
    is_dislike = serializers.BooleanField(default=False)               
    like_count = serializers.IntegerField()
    dislike_count = serializers.IntegerField()
    answer_count = serializers.IntegerField()
    image = serializers.CharField()
    tags = serializers.JSONField()

    def get_created_at(self, obj):
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%Y-%m-%d %H:%M:%S')


class CreateQuestionSerializer(serializers.Serializer):
    tags = serializers.JSONField()  
    description = serializers.CharField(allow_null=False, allow_blank=False) 


class CreateAnswerSerializer(serializers.Serializer):
    description = serializers.CharField(allow_null=False, allow_blank=False) 


class AnswerSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user_profile = UserProfileSerializer()
    created_at = serializers.SerializerMethodField(read_only=True)
    description = serializers.CharField()
    is_like = serializers.BooleanField(default=False)
    is_dislike = serializers.BooleanField(default=False)     
    like_count = serializers.IntegerField()
    dislike_count = serializers.IntegerField()
    def get_created_at(self, obj):
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%Y-%m-%d %H:%M:%S')


class LikeUserSerializer(serializers.Serializer):
    user_profile = UserProfileSerializer()
    created_at = serializers.SerializerMethodField(read_only=True)

    def get_created_at(self, obj):
        return (obj.created_at + datetime.timedelta(hours=3)).strftime('%d-%m-%Y %H:%M:%S')

