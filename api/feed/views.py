from datetime import datetime, timedelta
from unicodedata import category
from django.db import transaction
from django.http import Http404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from account.models import UserProfile
from api.feed.serializers import CreateAnswerSerializer, QuestionSerializer, CreateQuestionSerializer, AnswerSerializer
from core.models import Activity
from feed.models import Answer, Question
from django.db.models import Q

class QuestionListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = QuestionSerializer

    @transaction.atomic
    def get_queryset(self):
        time_filter = self.kwargs['time_filter'] 
        filterTagId = int(self.kwargs['filterTagId']) 
        start_index = int(self.kwargs['take_data_count'])
        next_data_count = 20
        if time_filter == "new": #new
          #  last_week = datetime.now()-timedelta(days=14)
           ## queryset = Question.objects.filter(created_at__gte=last_week.date()).order_by("-created_at")
           queryset = Question.objects.all().order_by("-created_at")
      #  elif time_filter == "weekly": #weekly
       #     last_week = datetime.now()-timedelta(days=datetime.today().weekday())
        #    queryset = Question.objects.filter(
         #      created_at__gte=last_week.date()
          #      ).order_by("-created_at")
        elif time_filter == "monthly": #monthly          
            queryset = Question.objects.filter(created_at__year=str(datetime.now().year), created_at__month=str(datetime.now().month)).order_by("-created_at")
        elif time_filter == "yearly": #yearly
            queryset = Question.objects.filter(created_at__year=str(datetime.now().year)).order_by("-created_at")
        else:
            queryset = Question.objects.all().order_by("-created_at")

        if filterTagId != -1:
            queryset = queryset.filter(
               Q(tags__0__id=filterTagId)|
               Q(tags__1__id=filterTagId)|
               Q(tags__2__id=filterTagId)|
               Q(tags__3__id=filterTagId)|
               Q(tags__4__id=filterTagId)
            )
        if queryset.count() > start_index:
            queryset = queryset[start_index:(start_index + next_data_count)]
        else:
            queryset = queryset[start_index + 1:(start_index + next_data_count)]

        for post in queryset:
            question_q = post.question_activities.filter(user_profile__user=self.request.user,category_type=Activity.QUESTION)
            post.is_like = question_q.filter(activity_type=Activity.UP_VOTE).exists()
            post.is_dislike = question_q.filter(activity_type=Activity.DOWN_VOTE).exists()
            post.like_count = question_q.filter(activity_type=Activity.UP_VOTE).count()
            post.dislike_count = question_q.filter(activity_type=Activity.DOWN_VOTE).count()
            post.answer_count = post.answers.count()

        return queryset


class CreateQuestionView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateQuestionSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = UserProfile.objects.get(user=self.request.user)
            Question.objects.create(
                description=serializer.validated_data['description'],
                user_profile=user,
                tags=serializer.validated_data['tags'],
            )
            return Response(status=200)

        return Response(status=404)


class CreateAnswerView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateAnswerSerializer

    def get_object(self):
        try:
            return UserProfile.objects.get(user=self.request.user)
        except UserProfile.DoesNotExist:
            raise Http404

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        profile = self.get_object()
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            Answer.objects.create(description=serializer.validated_data['description'], user_profile=profile, question_id=self.kwargs.get("id"))
            return Response(status=200)
        return Response(status=404)


class DeleteQuestionView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return Question.objects.get(id=id)
        except Question.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        post = self.get_object(self.kwargs.get("id"))
        post.delete()
        return Response(status=200)


class DeleteAnswerView(APIView):
    permission_classes = (AllowAny,)
    def get_object(self, id):
        try:
            return Answer.objects.get(id=id)
        except Answer.DoesNotExist:
            raise Http404

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        post = self.get_object(self.kwargs.get("id"))
        post.delete()
        return Response(status=200)



class QuestionAnswerListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = AnswerSerializer

    @transaction.atomic
    def get_queryset(self):
        queryset = Answer.objects.filter(question_id=self.kwargs.get("id")).order_by("-created_at")
              
        for answer in queryset:
            answer_q = answer.answer_activities.filter(user_profile__user=self.request.user, category_type=Activity.ANSWER)
            answer.is_like = answer_q.filter(activity_type=Activity.UP_VOTE).exists()
            answer.is_dislike = answer_q.filter(activity_type=Activity.DOWN_VOTE).exists()
            answer.like_count = answer_q.filter(activity_type=Activity.UP_VOTE).count()
            answer.dislike_count = answer_q.filter(activity_type=Activity.DOWN_VOTE).count()
        return queryset
