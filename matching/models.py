from django.db import models
from account.models import UserProfile

from core.models import BaseModel
from django.contrib.postgres.fields.jsonb import JSONField as JSONBField
from django.utils.translation import ugettext as _
from django.utils import timezone

class MatchingLike(BaseModel):
    liked_by = models.ForeignKey('matching.PetProfile', on_delete=models.CASCADE, related_name='matching_likes_by')
    liked_to = models.ForeignKey('matching.PetProfile', on_delete=models.CASCADE, related_name='matching_likes_to')


class PetProfile(BaseModel):

    MALE = 1
    FEMALE = 2
    GENDER_CHOICES = (
        (MALE, _('Erkek',),),
        (FEMALE, _('Dişi',),),
    )
    CAT = 1
    DOG = 2
    BIRD = 3
    FISH = 4
    REPTILE = 5
    RODENT = 6
    RABBIT =7
    TYPE_CHOICES = (
        (CAT, _('Kedi',),),
        (DOG, _('Köpek',),),
        (BIRD, _('Kuş',),),
        (FISH, _('Balık',),),
        (REPTILE, _('Sürüngen',),),
        (RODENT, _('Kemirgen',),),
        (RABBIT, _('Tavşan',),),
    )
    
    user_profile = models.ForeignKey(UserProfile, related_name='pet_profiles', on_delete=models.CASCADE,)
    pet_profile_image = models.TextField(_(u'Pet Profil Resmi'), blank=True, null=True)
    pet_name = models.CharField(_(u'İsim'), blank=False, max_length=25,)
    pet_type = models.SmallIntegerField(_(u'Tür'), default=MALE, choices=TYPE_CHOICES)
    pet_gender = models.SmallIntegerField(_(u'Cinsiyet'), default=MALE, choices=GENDER_CHOICES)
    pet_breed = models.SmallIntegerField(_(u'Irk'))
    pet_birthdate = models.DateField(_(u'Age'), default=timezone.now)
    description = models.CharField(_(u'Açıklama'), blank=True, null=True, max_length=300,)
    is_matching = models.BooleanField(_(u'Eşleştirme Durumu'), default=False, blank=False, null=False)

    def __str__(self):
        return "{}-{}".format(self.id, self.pet_name)

    def get_capitalize_pet_name(self):
        return "{}".format(self.pet_name.capitalize())

class PetWeight(BaseModel):
    pet_profile = models.ForeignKey('matching.PetProfile', on_delete=models.CASCADE, related_name='pet_weights')
    weight = models.FloatField(_(u'Weight'))

class PetAlarm(BaseModel):

    EAT = 1
    CARE = 2
    CLEAN = 3
    PILL = 4
    OTHER = 5
    TYPE_CHOICES = (
        (EAT, _('Gıda',),),
        (CARE, _('Bakım',),),
        (CLEAN, _('Temizlik',),),
        (PILL, _('İlaç',),),
        (OTHER, _('Diğer',),),
    )

    pet_profile = models.ForeignKey('matching.PetProfile', on_delete=models.CASCADE, related_name='pet_alarms')
    category = models.SmallIntegerField(_(u'Tür'), default=EAT, choices=TYPE_CHOICES)
    description = models.CharField(_(u'Açıklama'), blank=True, null=True, max_length=300,)
    days = JSONBField(default=list, null=False, blank=False)
    time = models.CharField(_(u'Saat'), blank=False, null=False, max_length=5,)
    isActive = models.BooleanField(_(u'Aktiflik'), default=True, blank=False, null=False)