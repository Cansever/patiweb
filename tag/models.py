from django.db import models
from django.utils.translation import ugettext as _
from core.models import BaseModel
from feed.models import Answer, Question
from django.db.models import Count, Q
# Create your models here.
class Tag(BaseModel):
    name = models.TextField(_(u'Etiket'), blank=False, null=False, max_length=15)    

    def get_tag_used_count(self):
        ques_count = Question.objects.filter(tags__icontains='"name": "{}"'.format(self.name)).count()
      #  ans_count = Answer.objects.filter(tags__icontains='"name": "{}"'.format(self.name)).count()

        return ques_count #+ ans_count 
