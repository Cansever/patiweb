from django.db import models
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
# Create your models here.
from core.models import Activity, BaseModel 
from django.contrib.postgres.fields.jsonb import JSONField as JSONBField
from django.contrib.contenttypes.fields import GenericRelation

class Question(BaseModel):
    user_profile = models.ForeignKey('account.UserProfile', on_delete=models.CASCADE, related_name='questions')
    description = models.CharField(_(u'Açıklama'), blank=False, null=False, max_length=300,)
    image = models.TextField(_(u'Resim'), blank=True, null=True)
    tags = JSONBField(default=list, null=False, blank=False)
    question_activities = GenericRelation(Activity, related_query_name='question_activities')
    
    def __str__(self):
        return "{}-u_id{}".format(self.id, self.profile.id)

class Answer(BaseModel):
    question = models.ForeignKey('feed.Question', on_delete=models.CASCADE, related_name='answers')
    description = models.TextField(_(u'Açıklama'), blank=False, null=False, max_length=300,)    
    user_profile = models.ForeignKey('account.UserProfile', on_delete=models.CASCADE, related_name='user_answers')
    answer_activities = GenericRelation(Activity, related_query_name='answer_activities')

    def __str__(self):
        return "{}-u_id{}".format(self.id, self.profile.id)
