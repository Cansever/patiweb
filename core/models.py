from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

class BaseModel(models.Model):
    created_at = models.DateTimeField(_('Oluşturulma Tarihi'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Güncellenme Tarihi'), auto_now=True)

    class Meta:
        abstract = True


class Activity(BaseModel):
    UP_VOTE = 'U'
    DOWN_VOTE = 'D'
    ACTIVITY_TYPES = (
        (UP_VOTE, 'Up Vote'),
        (DOWN_VOTE, 'Down Vote'),
    )

    QUESTION = 'Q'
    ANSWER = 'A'
    BLOG = 'B'
    ADOPTPOST = 'AP'

    CATEGORY_TYPES = (
        (QUESTION, 'QUESTION'),
        (ANSWER, 'ANSWER'),
        (BLOG, 'BLOG'),
        (ADOPTPOST, 'ADOPTPOST'),
    )

    user_profile = models.ForeignKey('account.UserProfile',null=True, on_delete=models.CASCADE, related_name='question_activities',)
    activity_type = models.CharField(max_length=2, choices=ACTIVITY_TYPES)
    category_type = models.CharField(max_length=2, choices=CATEGORY_TYPES)  

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    