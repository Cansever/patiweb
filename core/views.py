from django.shortcuts import render

from account.models import UserProfile
from django.db import transaction
from django.http import Http404
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from account.models import UserProfile 
from core.models import Activity
from feed.models import Answer, Question
from adoption.models import AdoptPost
from django.contrib.contenttypes.models import ContentType
from rest_framework import generics

from tag.models import Tag

class ErrorAPICode():
    WRONG_USERNAME = "1000"
    WRONG_EMAIL = "1001"
    INVALID_AUTH = "1002"
    INVALID_PASS = "1003"
    ALREADY_EXIST_USERNAME = "1004"
    ALREADY_EXIST_EMAIL = "1005"
    ALREADY_EXIST_ACCOUNT = "1006"
    ALREADY_EXIST_TAG = "1007"

class ActivityView(APIView):
    permission_classes = (AllowAny,)

    def get_object(self):
        try:
            return UserProfile.objects.get(user=self.request.user)
        except UserProfile.DoesNotExist:
            raise Http404

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        profile = self.get_object()
        id = self.kwargs['id']
        activity_type = self.kwargs['activity_type']
        category_type = self.kwargs['category_type']
        if category_type == Activity.QUESTION:
            obj = Question.objects.get(id=id)        
            ct = ContentType.objects.get_for_model(obj)
            question_qset = Activity.objects.filter(user_profile=profile, content_type=ct, object_id=id, category_type=category_type)
            if question_qset.exists():
                question_qset.delete()
                Activity.objects.create(user_profile=profile, content_object=obj, category_type=category_type, activity_type=activity_type) 
            else:
                Activity.objects.create(user_profile=profile, content_object=obj, category_type=category_type, activity_type=activity_type)
        elif category_type == Activity.ANSWER:       
            obj = Answer.objects.get(id=id)  
            ct = ContentType.objects.get_for_model(obj)       
            answer_qset = Activity.objects.filter(user_profile=profile, content_type=ct, object_id=id, category_type=category_type)
            if answer_qset.exists():
                answer_qset.delete()
                Activity.objects.create(user_profile=profile, content_object=obj, category_type=category_type, activity_type=activity_type)
            else:
                Activity.objects.create(user_profile=profile, content_object=obj, category_type=category_type, activity_type=activity_type)
        elif category_type == Activity.ADOPTPOST:   
            obj = AdoptPost.objects.get(id=id) 
            ct = ContentType.objects.get_for_model(obj)             
            adopt_qset = Activity.objects.filter(user_profile=profile, content_type=ct, object_id=id, category_type=category_type)
            if adopt_qset.exists():
                adopt_qset.delete()
                Activity.objects.create(user_profile=profile, content_object=obj, category_type=category_type,activity_type=activity_type)    
            else:
                Activity.objects.create(user_profile=profile, content_object=obj, category_type=category_type,activity_type=activity_type)
        
        return Response(status=200)


 